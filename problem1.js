/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require('fs')
const path = require('path')
const fileNames = []

function createDirectory() {

    return new Promise((resolve, reject) => {
        const folderName = 'randomFolder'
        fs.mkdir(folderName, (err) => {
            if (err) {
                reject(err)
            } else {
                console.log('Folder created successfully')
                resolve(folderName)
            }
        })
    })

}

const writefile = (folderName) => {

    return new Promise((resolve, reject) => {

        const fileData = { "employee": { "name": "sonoo", "salary": 56000, "married": true } }

        for (let index = 1; index < 11; index++) {

            const fileName = `A${index}.json`
            fileNames.push(fileName)
            let filePath = path.join(folderName,fileName)

            fs.writeFile(filePath, JSON.stringify(fileData), (err) => {
                if (err) {

                    reject(err)

                } else {

                    console.log(`File ${fileName} created`)

                    if (index==10){
                        resolve(folderName)
                    }   
                }
            })
        }
        
    })

}

const deletefile = (folderPath) => {

    return new Promise((resolve, reject) => {
        let index =0

        fileNames.forEach((fileName) => {

            fs.unlink(path.join(folderPath,fileName), (err) => {
                if (err) {
                    reject(err)
                } else {

                    console.log(`Delete ${fileName}`)
                    index++

                    if (index == fileNames.length){

                        resolve('Delete all the file successfully')

                    }
                }
            })
        })      
    })
}

function problem1() {

    createDirectory()
        .then((folderName) => {
            return writefile(folderName)
        })
        .then((path) => {
            return deletefile(path)
        })
        .then((data) => console.log(data))
        .catch((err) => console.error(err))
}

module.exports = problem1