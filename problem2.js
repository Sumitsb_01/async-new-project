// /*1. Read the given file lipsum.txt
// 2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
// 3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
// 4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
// 5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
// */

const fs = require('fs')


function readfile(fileName){

    return new Promise((resolve,reject) =>{

        fs.readFile(fileName,'utf-8',(err,data) =>{

            if(err){
                reject(err)
            }else{
                resolve(data)
            }
        })
    }) 
}

function writefile(fileData,fileName){

    return new Promise((resolve,reject) => {

        fs.writeFile(fileName,fileData,(err) =>{
            if(err){
                reject(err)
            }else{
                resolve(fileName)
            }
        })
    })
}

function appendfile(fileName) {

    return new Promise((resolve,reject) => {
        
        fs.appendFile('filenames.txt',`${fileName}`,(err) => {
            if(err){
                reject(err)
            }else{
                resolve(fileName.trim('\n'))
            }
        })
    })
}

function Deletefile(FileNamesData) {

    return new Promise((resolve, reject) => {

        const namesArr = FileNamesData.split('\n')
        let index = 0

        namesArr.forEach(filename => {

            fs.unlink(filename, (err) => {

                if (err) {

                    reject(err)

                } else {

                    console.log(`Delete ${filename} file`)
                    index++

                    if (index == namesArr.length) {

                        resolve('Successfully Deleted all the files')

                    }
                }
            })
        })
    })
}


function problem2(){
    readfile('lipsum.txt')
    .then((upperCaseData) => {

        console.log('Successfully read lipsum.txt file')
        upperCaseData = upperCaseData.toUpperCase()
        return writefile(upperCaseData,'new1.txt')

    }).then((fileName) =>{

        console.log('Enter the data of lipsum in new1.txt file in Upper Case')
        return appendfile(`${fileName}\n`)

    }).then((fileName) =>{

        console.log(`write ${fileName} in filenames.txt`)
        return readfile(fileName)

    }).then((lowerCaseData) => {

        console.log('Read the file new1.txt')
        lowerCaseData = lowerCaseData.toLowerCase().replaceAll('\n','').split('. ').join('\n')
        return writefile(lowerCaseData,'new2.txt')

    }).then((fileName) => {

        console.log('Enter the data of new1.txt in new2.txt file in Lower Case')
        return appendfile(`${fileName}\n`)

    }).then((fileName) => {

        console.log(`write ${fileName} in filenames.txt`)
        return readfile(fileName)

    }).then((sortData) =>{

        console.log('Read the file new1.txt')
        sortData =  sortData.split('\n').sort().join('\n')
        return writefile(sortData,'new3.txt')

    }).then((fileName) => {

        console.log('Enter the data of new2.txt in new3.txt file')
        return appendfile(`${fileName}`)

    }).then((fileName) => {

        console.log(`write ${fileName} in filenames.txt`)
        return readfile('filenames.txt')

    }).then((data) => {

        return Deletefile(data)

    }).then((data) => {

        console.log(data)

    }).catch((err) => {

        console.error(err)

    })
}

module.exports =problem2




